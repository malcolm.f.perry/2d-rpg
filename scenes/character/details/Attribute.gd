extends Node
class_name Attribute

signal level_up

# The name of the attribute
var attribute_name:String = ""
# The current amount of experience
var current_experience:int = 0
# The target experience to reach to level up
var target_experience:int = 100
# On level, increase required experience by percentage
var experience_step:float = .25
# The current level
var current_level:int = 1
# Bonus levels given by equipment, buffs, etc.
var bonus_levels:int = 0

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass

# Initialize the attribute variables
func init(name:String, current_xp:int, target_xp:int, level:int) -> void:
	attribute_name = name
	current_experience = current_xp
	target_experience = target_xp
	current_level = level

# Add experience to the attribute
func add_experience(experience:int) -> void:
	current_experience += experience
	
	# Check if we have leveled up the attribute
	while current_experience >= target_experience:
		# reset current xp to overflow amount
		current_experience = current_experience - target_experience		
		# increase target xp
		target_experience += int((target_experience * experience_step))
		# send level up signal
		emit_signal("level_up")

# Add bonus levels to the attribute
func add_bonus_levels(bonus:int) -> void:
	bonus_levels += bonus

# Remove bonus levels from the attribute
func remove_bonus_levels(bonus:int) -> void:
	bonus_levels -= bonus

# Get the current level
func get_current_level() -> int:
	return current_level

# Get the current level plus any bonus levels
func get_total_level() -> int:
	return current_level + bonus_levels
