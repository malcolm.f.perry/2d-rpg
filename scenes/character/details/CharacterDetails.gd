extends Node
class_name CharacterDetails

signal health_change
signal energy_change

# Holds all of the core stats for characters

# Core
var character_name:String = ""

# Stats
var health:int = 0
var energy:int = 0
var ether:int = 0

# Resistances
var crushing_resistance = 0

# Attributes
var might:Attribute = Attribute.new()
var finesse:Attribute = Attribute.new()
var endurance:Attribute = Attribute.new()
var vigor:Attribute = Attribute.new()

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	might.init("Might", 0, 100, 1)
	finesse.init("Finesse", 0, 100, 1)
	endurance.init("Endurance", 0, 100, 1)
	vigor.init("Vigor", 0, 100, 1)

# Initialize class variables
func init() -> void:
	health = get_max_health()
	energy = get_max_energy()

# Apply a change of the given amount to the given stat
func apply_stat_change(stat:int, change_amount:int) -> void:
	if stat == Globals.STAT.Health:
		health += change_amount
		emit_signal("health_change")
		check_death()
	elif stat == Globals.STAT.Energy:
		energy += change_amount
		emit_signal("energy_change")
	elif stat == Globals.STAT.Ether:
		ether += change_amount

# Check whether this character is still alive
func check_death() -> void:
	if health <= 0:
		# signal that this character died
		print(character_name, " has died!")

# Get the stat by enum
func get_stat(stat_enum:int) -> int:
	if stat_enum == Globals.STAT.Health:
		return health
	elif stat_enum == Globals.STAT.Energy:
		return energy
	elif stat_enum == Globals.STAT.Ether:
		return ether
	else:
		print("Could not find stat for enum: ", stat_enum)
		return -1

# Get the initiative bonus
func get_initiative() -> int:
	return int(ceil(finesse.current_level / 5.0))

# Get the movement speed
func get_movement_speed() -> int:
	return int(ceil(finesse.current_level / 5.0))

# Get the max possible health value
func get_max_health() -> int:	
	return vigor.current_level * 100

# Get the max possible energy value
func get_max_energy() -> int:
	return endurance.current_level * 100

# Get the current health percentage remaining
func get_health_percent() -> int:
	return int(100 * health / get_max_health())

# Get the current energy percentage remaining
func get_energy_percent() -> int:
	return int(100 * energy / get_max_energy())

# Get the current ether percentage remaining
func get_ether_percent() -> int:
	return 0

# Get the resistance for the given damage type
func get_resistance(damage_type:int) -> int:
	if damage_type == Globals.EFFECT_TYPE.Crushing:
		return crushing_resistance
	return 0 
