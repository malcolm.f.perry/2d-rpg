extends Node
class_name Ability

# Class used to describe abilities that characters and items have. Abilities will
# apply an arbitrarily long list of effects to a target.

# The name of the ability
var ability_name:String = ""
# Path to the action bar icon
var icon:String = ""
# {weapon} Optional reference to the weapon object
# TODO: Add weapon type
var owning_weapon = null
# {affinity enum} What elemental affinity this ability has
var affinity:int = Globals.AFFINITY.None
# {ability_type enum} Whether the ability applies effects immediately, periodically etc.
var type:int = Globals.ABILITY_TYPE.Instant
# How long to wait before applying the ability
var delay:int = 0
# How far in tiles the ability will reach (0 is the origin tile, 1 is the adjacent neighbors, etc)
var ability_range:int = 1
# How many turns before this ability can be used again (0 used again in the same turn, 1 can be used next turn, etc.)
var cooldown:int = 1
# How many turn action points it takes to activate this ability
var action_point_cost:int = 1
# {[] Effect} The list of effects for this ability to apply
var effects:Array = []

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.

# Initialize the ability variables
func init(new_name:String, new_affinity:int, new_type:int, new_delay:int, 
		new_ability_range:int, new_cooldown:int, new_ap_cost:int, new_effects:Array = []) -> void:
	validate_and_set(Globals.ABILITY_ATTRIBUTE.Name, new_name)
	validate_and_set(Globals.ABILITY_ATTRIBUTE.Affinity, new_affinity)
	validate_and_set(Globals.ABILITY_ATTRIBUTE.Type, new_type)
	validate_and_set(Globals.ABILITY_ATTRIBUTE.Delay, new_delay)
	validate_and_set(Globals.ABILITY_ATTRIBUTE.Ability_Range, new_ability_range)
	validate_and_set(Globals.ABILITY_ATTRIBUTE.Cooldown, new_cooldown)
	validate_and_set(Globals.ABILITY_ATTRIBUTE.AP_Cost, new_ap_cost)
	validate_and_set(Globals.ABILITY_ATTRIBUTE.Effects, new_effects)

# Validate the attribute and set its value
func validate_and_set(attribute_name:int, value) -> void:
	if attribute_name == Globals.ABILITY_ATTRIBUTE.Name and typeof(value) == TYPE_STRING:
		ability_name = value
	elif attribute_name == Globals.ABILITY_ATTRIBUTE.Affinity and Globals.has_enum_value(Globals.AFFINITY, value):
		affinity = value
	elif attribute_name == Globals.ABILITY_ATTRIBUTE.Type and Globals.has_enum_value(Globals.ABILITY_ATTRIBUTE, value):
		type = value
	elif attribute_name == Globals.ABILITY_ATTRIBUTE.Delay and value >= 0:
		delay = value
	elif attribute_name == Globals.ABILITY_ATTRIBUTE.Ability_Range and value >= 0:
		ability_range = value
	elif attribute_name == Globals.ABILITY_ATTRIBUTE.Cooldown and value >= 0:
		cooldown = value
	elif attribute_name == Globals.ABILITY_ATTRIBUTE.AP_Cost and value >= 0:
		action_point_cost = value
	elif attribute_name == Globals.ABILITY_ATTRIBUTE.Effects:
		effects = value
	else:
		print("Ability attribute: ", Globals.ABILITY_ATTRIBUTE.keys()[attribute_name], " could not be set with value: ", value)

# Add a new effect to the ability
func add_effect(new_power:int, new_penetration:int, new_accuracy:int, new_size:int, 
		new_duration:int, new_cost:Dictionary, new_type:int) -> void:
	var effect = Effect.new()
	effect.init(new_power, new_penetration, new_accuracy, new_size, 
		new_duration, new_cost, new_type)
	effects.append(effect)

# Add up the cost of all the different effects to get the total cost of the ability
func get_total_cost_map() -> Dictionary:
	var cost_map = Effect.create_effect_cost_map(0, 0, 0)
	for effect in effects:
		cost_map[Globals.STAT.Health] += effect.cost[Globals.STAT.Health]
		cost_map[Globals.STAT.Energy] += effect.cost[Globals.STAT.Energy]
		cost_map[Globals.STAT.Ether] += effect.cost[Globals.STAT.Ether]
	return cost_map
