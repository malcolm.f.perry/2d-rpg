extends Node
class_name Effect

# Describes what an ability will do when activated

# How powerful the Effect is
var power:int = 1
# How much an Effect will penetrate Armor / Resistance (based on damage type)
var penetration:int = 0
# How accurate the effect is
var accuracy:int = 0
# How large the effect is in tiles (1 is only the targeted tile, each increase adds adjacent neighbors)
var size:int = 1
# How long the effect lasts in turns (0 is only the turn the effect is applied in)
var duration:int = 0
# {stat enum} What stat and how much does this stat need to be applied
var cost:Dictionary = {Globals.STAT.Health : 0, Globals.STAT.Energy : 0, Globals.STAT.Ether : 0}
# {effect_type enum} What type of Effect (determines healing/damaging/resistances etc.)
var type:int

# Create a cost map for an effect
static func create_effect_cost_map(health_cost:int, energy_cost:int, ether_cost:int) -> Dictionary:
	return {Globals.STAT.Health : clamp(health_cost, -100, 0), 
			Globals.STAT.Energy : clamp(energy_cost, -100, 0), 
			Globals.STAT.Ether : clamp(ether_cost, -100, 0)}

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.

# Initialize the Effect variables
func init(new_power:int, new_penetration:int, new_accuracy:int, new_size:int, 
		new_duration:int, new_cost:Dictionary, new_type:int) -> void:
	validate_and_set(Globals.EFFECT_ATTRIBUTE.Power, new_power)
	validate_and_set(Globals.EFFECT_ATTRIBUTE.Penetration, new_penetration)
	validate_and_set(Globals.EFFECT_ATTRIBUTE.Accuracy, new_accuracy)
	validate_and_set(Globals.EFFECT_ATTRIBUTE.Size, new_size)
	validate_and_set(Globals.EFFECT_ATTRIBUTE.Duration, new_duration)
	validate_and_set(Globals.EFFECT_ATTRIBUTE.Cost, new_cost)
	validate_and_set(Globals.EFFECT_ATTRIBUTE.Type, new_type)

# Validate the attribute and set its value
func validate_and_set(attribute_name:int, value) -> void:
	if attribute_name == Globals.EFFECT_ATTRIBUTE.Power and value > 0:
		power = value
	elif attribute_name == Globals.EFFECT_ATTRIBUTE.Penetration and value >= 0:
		penetration = value
	elif attribute_name == Globals.EFFECT_ATTRIBUTE.Accuracy and value >= 0:
		accuracy = value
	elif attribute_name == Globals.EFFECT_ATTRIBUTE.Size and value > 0:
		size = value
	elif attribute_name == Globals.EFFECT_ATTRIBUTE.Duration and value >= 0:
		duration = value
	elif attribute_name == Globals.EFFECT_ATTRIBUTE.Cost:
		cost = value
	elif attribute_name == Globals.EFFECT_ATTRIBUTE.Type and Globals.has_enum_value(Globals.EFFECT_TYPE, value):
		type = value
	else:
		print("Effect attribute: ", Globals.EFFECT_ATTRIBUTE.keys()[attribute_name], 
			" could not be set with value: ", value)
