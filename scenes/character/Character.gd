extends Area2D
class_name Character

# The character's current grid position
var grid_position:Vector2 = Vector2(-1, -1)
# The stat attribute details for the character
var character_details:CharacterDetails = CharacterDetails.new()
# The list of abilities this character has
var ability_list:Array = []
# The health bar display of this character
onready var health_bar = $StatBars/HealthBar
# The energy bar display of this character
onready var energy_bar = $StatBars/EnergyBar

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	randomize()
	add_basic_abilities()
	character_details.init()
	character_details.connect("health_change", self, "update_health_bar")
	character_details.connect("energy_change", self, "update_energy_bar")
	health_bar.value = character_details.get_health_percent()
	energy_bar.value = character_details.get_energy_percent()

# Get the name of this character from the character details
func get_character_name() -> String:
	return character_details.character_name

# Set the character's name
func set_character_name(character_name:String) -> void:
	character_details.character_name = character_name
	$Name.text = character_name

# A the basic abilities all characters share
func add_basic_abilities() -> void:
	# Add punch ability
	var punch_ability = Ability.new()
	punch_ability.init("Punch", Globals.AFFINITY.None, Globals.ABILITY_TYPE.Instant, 0, 1, 1, 1)
	punch_ability.add_effect(1, 0, 0, 1, 0, Effect.create_effect_cost_map(0, -10, 0), Globals.EFFECT_TYPE.Crushing)
	ability_list.append(punch_ability)

# Check if the ability at the specified index can be activated
func can_activate_ability(ability_index:int) -> bool:
	if ability_index >= 0 and ability_index < ability_list.size():
		var ability = ability_list[ability_index]
		var cost_map = ability.get_total_cost_map()
		if cost_map[Globals.STAT.Health] > character_details.get_stat(Globals.STAT.Health):
			return false
		if cost_map[Globals.STAT.Energy] > character_details.get_stat(Globals.STAT.Energy):
			return false
		if cost_map[Globals.STAT.Ether] > character_details.get_stat(Globals.STAT.Ether):
			return false
		return true
	return false

# Get the ability at the given index
func get_ability(index:int) -> Ability:
	return ability_list[index]

# Apply the cost of an ability at the given index to the stats of this character
func apply_ability_cost(index:int) -> void:
	var ability = get_ability(index)
	var cost_map = ability.get_total_cost_map()
	for stat in cost_map.keys():
		var cost = cost_map.get(stat)
		print("stat ", stat, " ", cost)
		if cost != 0:
			apply_stat_change(stat, cost)

# Apply a change of the given amount to the given stat
func apply_stat_change(stat:int, change_amount:int) -> void:
	character_details.apply_stat_change(stat, change_amount)

# Update the health bar percentage value
func update_health_bar() -> void:
	health_bar.value = character_details.get_health_percent()

# Update the energy bar percentage value
func update_energy_bar() -> void:
	energy_bar.value = character_details.get_energy_percent()

# Update the character location to the specified grid and world position
func move(grid_pos:Vector2, position_vector:Vector2) -> void:
	grid_position = grid_pos
	position = position_vector

# Get the initiative roll for this character
func get_initiative() -> int:
	return int(ceil(rand_range(1, 10)) + character_details.get_initiative())

# Get the movement speed of this character
func get_movement_speed() -> int:
	return character_details.get_movement_speed()

# Get the maximum number of action points a character has per turn
func get_max_ap() -> int:
	return 2

# Get the percentage of health remaining from the character details
func get_health_percent() -> int:
	return character_details.get_health_percent()

# Get the percentage of energy remaining from the character details
func get_energy_percent() -> int:
	return character_details.get_energy_percent()

# Get the percentage of Ether remaining from the character details
func get_ether_percent() -> int:
	return character_details.get_ether_percent()

# Get the character's resistance to the given damage type
func get_resistance(damage_type:int) -> int:
	return character_details.get_resistance(damage_type)
