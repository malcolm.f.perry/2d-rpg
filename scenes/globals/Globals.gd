extends Node

# Core enums
enum STAT {Health, Energy, Ether}
enum ATTRIBUTE {Might, Finesse, Endurance, Vigor}
enum AFFINITY {None, Fire, Water, Earth, Air, Spirit}

# Ability enums
enum ABILITY_ATTRIBUTE {Name, Affinity, Type, Delay, Ability_Range, Cooldown, AP_Cost, Effects}
enum ABILITY_TYPE {Instant}

# Effect enums
enum EFFECT_ATTRIBUTE {Power, Penetration, Accuracy, Size, Duration, Cost, Type}
enum EFFECT_TYPE {Slashing, Crushing, Piercing, Burning, Freezing, Sonic,
					Recover_Health,	Recover_Energy, Recover_Ether}

# Tile enums
enum TILE_FILTERS {CanOccupy, NoRootTile}

# Global Damage Die
var damage_die = 6

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	randomize()

# Check if the enum integer value is contained with the enum
func has_enum_value(base_enum:Dictionary, value:int) -> bool:
	return value >= 0 and value < base_enum.keys().size()

# Roll a number of damage dice determined by the passed parameter
func roll_damage_dice(num_dice:int) -> int:
	var dmg = 0
	for roll in num_dice:
		dmg += int(rand_range(1, damage_die))
	return dmg
