extends VBoxContainer

# Whether this plaque should show a remaining AP display
export var shows_ap:bool = false

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	if !shows_ap:
		$AP.hide()

# Update the character info that backs this plaque
func update_turn_details(character:Character) -> void:
	$Name.text = character.get_character_name()

# Update the AP display
func update_ap(new_ap:int) -> void:
	$AP.text = "AP: " + String(new_ap)
