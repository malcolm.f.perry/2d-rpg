extends Button

signal ability_button_pressed

# The index of the button in the ability list
export var index:int = -1
# The ability this button represents
var ability:Ability = null

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	$Index.text = get_text_for_index()

# Called when this button is pressed
func _on_Button_pressed() -> void:
	emit_signal("ability_button_pressed", index)

# Get the text for this button
func get_text_for_index() -> String:
	if index == 9:
		return "0"
	else:
		return String(index + 1)

# Update the ability this button holds
func update_ability(new_ability:Ability) -> void:
	ability = new_ability
	text = ability.ability_name
