extends CanvasLayer

signal toggle_movement_mode
signal end_turn_button_pressed
signal ability_button_pressed

# The reference for the ability button scene
var ability_button_ref:Resource = preload("res://scenes/ui/AbilityButton.tscn")
# Turn queue panels
onready var turn_queue_panels:HBoxContainer = $MainPanel/HBoxContainer/TurnControlsPanel/TurnInfoPanel/TurnQueue
# Ability hotbar
onready var hotbar:GridContainer = $MainPanel/ActionsPanel/Hotbar
# Move state toggle button
onready var move_state_toggle:Button = $MainPanel/ActionsPanel/MoveButton

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	toggle_player_ui_disabled(true)

# Called when the move toggle is pressed
func _on_MoveButton_pressed() -> void:
	emit_signal("toggle_movement_mode")

# Called when the end turn button is pressed
func _on_EndTurnButton_pressed() -> void:
	emit_signal("end_turn_button_pressed")

# Called when an ability button is pressed
func _on_ability_button_pressed(index) -> void:
	emit_signal("ability_button_pressed", index)

# Enable/Disable the player UI
func toggle_player_ui_disabled(active:bool) -> void:
	reset_move_toggle()
	move_state_toggle.disabled = active
	for ability_button in hotbar.get_children():
		ability_button.disabled = active

# Reset the move toggle button
func reset_move_toggle() -> void:
	move_state_toggle.set_pressed(false)

# Update the AP display
func update_remaining_ap(new_ap:int) -> void:
	turn_queue_panels.get_child(0).update_ap(new_ap)

# Update the turn queue display to reflect the given array
func update_turn_queue(turn_queue:Array, turn_pointer:int) -> void:
	# TODO: show the next few characters in the turn queue
	var index = turn_pointer
	for turn_plaque in turn_queue_panels.get_children():
		# Check if we need to reset the index counter
		if index >= turn_queue.size():
			index = 0
		turn_plaque.update_turn_details(turn_queue[index].character)
		index+=1

# Add an ability to the hotbar UI
func add_ability_to_hotbar(index:int, ability:Ability) -> void:
	if index >= 0 and index < hotbar.get_child_count():
		var ability_button = hotbar.get_child(index)
		ability_button.update_ability(ability)
