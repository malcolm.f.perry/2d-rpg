extends Node

enum PLAYER_INPUT_STATE {VIEW, MOVE, ACTIVATE_ABILITY}

# The current input state of the player
var player_input_state:int = PLAYER_INPUT_STATE.VIEW
# The player scene
var player_scene_ref:Resource = preload("res://scenes/character/player/Player.tscn")
# The player character
var player:Player = null
# The queue of Actors for each turn
var turn_queue = []
# The pointer for the turn queue
var turn_pointer = 0
# The NPC scene
var npc_scene_ref = preload("res://scenes/character/npc/Npc.tscn")
# The map of all NPCs in the level
var npcs = {}

# Static function to sort the turn queue based on initiative
static func sort_initiative_order(a:Dictionary, b:Dictionary) -> bool:
	if a.initiative > b.initiative:
		return true
	return false

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	randomize()
	# TODO: add add_character function to handle npc and player add to map with position
	player = player_scene_ref.instance()
	player.set_character_name("Player")
	add_child(player)
	$TileMap.update_character_position(player, Vector2(1, 2))
	turn_queue.append(make_turn_object(player))
	$HUD.add_ability_to_hotbar(0, player.ability_list[0])
	
	var npc_1 = npc_scene_ref.instance()
	npc_1.set_character_name("NPC_1")
	npcs["npc_1"] = npc_1
	add_child(npc_1)
	$TileMap.update_character_position(npc_1, Vector2(8, 2))
	turn_queue.append(make_turn_object(npc_1))
	
	start_round()

# Called to toggle the player input state
func _on_HUD_toggle_movement_mode() -> void:
	if is_characters_turn(player):
		var current_turn_obj = get_current_turn_object()
		if player_input_state == PLAYER_INPUT_STATE.MOVE:
			change_player_input_state(PLAYER_INPUT_STATE.VIEW)
		elif current_turn_obj.current_ap > 0:
			change_player_input_state(PLAYER_INPUT_STATE.MOVE)
			show_tiles_for_player_move()

# Called to end the player's turn
func _on_HUD_end_turn_button_pressed() -> void:
	_on_HUD_toggle_movement_mode()
	end_turn(player)

# Called when a tile is clicked by the player
func _on_TileMap_tile_clicked(grid_pos:Vector2) -> void:
	if can_player_move(grid_pos):
		commit_player_move(grid_pos)
	elif can_player_activate_ability(grid_pos):
		commit_player_ability(grid_pos)

# Called when an ability button is pressed
func _on_HUD_ability_button_pressed(index:int) -> void:
	if can_player_queue_ability(index):
		change_player_input_state(PLAYER_INPUT_STATE.ACTIVATE_ABILITY)
		get_current_turn_object().pending_ability_index = index
		var ability = player.get_ability(index)
		get_current_turn_object().allowed_ability_positions = $TileMap.get_neighbors(player.grid_position,
		ability.ability_range, [], $TileMap.highlight_tile_ref)

# Called to update the player input state
func change_player_input_state(state:int) -> void:
	if state == PLAYER_INPUT_STATE.VIEW:
		player_input_state = PLAYER_INPUT_STATE.VIEW
		$HUD.reset_move_toggle()
	elif state == PLAYER_INPUT_STATE.MOVE:
		player_input_state = PLAYER_INPUT_STATE.MOVE
	elif state == PLAYER_INPUT_STATE.ACTIVATE_ABILITY:
		player_input_state = PLAYER_INPUT_STATE.ACTIVATE_ABILITY
		$HUD.reset_move_toggle()
	var turn_obj = get_current_turn_object()
	$TileMap.reset_tile_textures(turn_obj.allowed_movement_positions)
	turn_obj.allowed_movement_positions = []
	turn_obj.pending_ability_index = -1
	$TileMap.reset_tile_textures(turn_obj.allowed_ability_positions)
	turn_obj.allowed_ability_positions = []

# Create a turn object wrapper for a character to hold turn information
func make_turn_object(character:Character) -> Dictionary:	
	return {"character": character, 
			"initiative": character.get_initiative(),
			"max_ap": character.get_max_ap(),
			"current_ap": character.get_max_ap(),
			"allowed_movement_positions": [],
			"pending_ability_index": -1,
			"allowed_ability_positions": []}

# Get the turn object for the current turn
func get_current_turn_object():
	return turn_queue[turn_pointer]

# Get the character whose turn it currently is
func get_current_acting_character() -> Character:
	return get_current_turn_object().character

# Start the turns
func start_round() -> void:
	turn_queue.sort_custom(self, "sort_initiative_order")
	do_next_turn()

# Move to the next character's turn
func do_next_turn() -> void:
	$HUD.update_turn_queue(turn_queue, turn_pointer)
	if is_characters_turn(player):
		start_player_turn()
	else:
		do_npc_turn()

# Reset state for the player's turn
func start_player_turn() -> void:
	var current_turn_obj = get_current_turn_object()
	# TODO: reset pending ability
	# reset AP
	# reset additional things
	current_turn_obj.current_ap = current_turn_obj.max_ap
	current_turn_obj.allowed_movement_positions = []
	$HUD.update_remaining_ap(current_turn_obj.current_ap)
	$HUD.toggle_player_ui_disabled(false)

# Check if it is a specified character's turn
func is_characters_turn(character:Character) -> bool:
	return get_current_acting_character() == character

# Increment or wrap the turn pointer
func advance_turn_pointer() -> void:
	print(turn_pointer)
	if (turn_pointer + 1) == turn_queue.size():
		turn_pointer = 0
	else:
		turn_pointer += 1

# End the turn for the specified Character
func end_turn(character:Character) -> void:
	if is_characters_turn(character):
		advance_turn_pointer()
		do_next_turn()
		if character == player:
			$HUD.toggle_player_ui_disabled(true)

# Update the allowed positions for a player's move and highlight the tiles
func show_tiles_for_player_move() -> void:
	get_current_turn_object().allowed_movement_positions = $TileMap.get_neighbors(player.grid_position,
		player.get_movement_speed(), [Globals.TILE_FILTERS.CanOccupy, Globals.TILE_FILTERS.NoRootTile], 
		$TileMap.highlight_tile_ref)

# Check whether the player is able to move
func can_player_move(grid_pos:Vector2) -> bool:
	return player_input_state == PLAYER_INPUT_STATE.MOVE and can_move(player, grid_pos)

# Check wheter the specified character is able to move
func can_move(character:Character, grid_pos:Vector2) -> bool:
	var turn_obj = get_current_turn_object()
	return (is_characters_turn(character) and turn_obj.current_ap > 0
		and turn_obj.allowed_movement_positions.has(grid_pos))

# Do updates resulting from the player's move
func commit_player_move(grid_pos:Vector2) -> void:
	var current_turn_obj = get_current_turn_object()
	current_turn_obj.current_ap -= 1
	$HUD.update_remaining_ap(current_turn_obj.current_ap)
	$TileMap.update_character_position(player, grid_pos)
	$TileMap.reset_tile_textures(current_turn_obj.allowed_movement_positions)
	if current_turn_obj.current_ap > 0:
		show_tiles_for_player_move()
	else:
		_on_HUD_toggle_movement_mode()

# Check whether the ability at the given index can be used
func can_player_queue_ability(index:int) -> bool:
	var turn_obj = get_current_turn_object()
	return (is_characters_turn(player) and turn_obj.current_ap > 0 and	player.can_activate_ability(index))

# Check whether the player can active the queued ability at the given location
func can_player_activate_ability(grid_pos:Vector2) -> bool:
	var turn_obj = get_current_turn_object()
	var index = turn_obj.pending_ability_index
	return (player_input_state == PLAYER_INPUT_STATE.ACTIVATE_ABILITY and 
		can_player_queue_ability(index) and turn_obj.allowed_ability_positions.has(grid_pos))

# Do updates resulting from the activation of the player's ability
func commit_player_ability(grid_pos:Vector2) -> void:
	var index = get_current_turn_object().pending_ability_index
	var ability = player.get_ability(index)
	var current_turn_obj = get_current_turn_object()
	current_turn_obj.current_ap -= ability.action_point_cost
	player.apply_ability_cost(index)
	apply_ability_to_targets(player, ability, grid_pos)
	$HUD.update_remaining_ap(current_turn_obj.current_ap)
	$TileMap.reset_tile_textures(current_turn_obj.allowed_ability_positions)
	change_player_input_state(PLAYER_INPUT_STATE.VIEW)

# Apply the activated ability's effects to the characters in the target grid position
func apply_ability_to_targets(source_character:Character, ability:Ability, grid_pos:Vector2) -> void:
	for effect in ability.effects:
		var affected_tiles = $TileMap.get_neighbors(grid_pos, effect.size - 1)
		var affected_characters = $TileMap.get_characters_in_tiles(affected_tiles)
		# Do hit checks and apply damage
		for target_character in affected_characters:
			if does_character_hit_target(source_character, target_character, ability, effect):
				if does_character_dodge(target_character):
					# TODO: Dodge case
					print("Dodge!")
				else:
					if does_character_block(target_character):
						# TODO: Block Case
						print("Block!")
					else:
						# Calculate what damage the effect did and apply it to the target
						calculate_and_apply_damage(source_character, target_character, ability, effect)
			else:
				# TODO: Miss Case
				print("Miss!")

# Check whether a target character is hit by a given ability effect
func does_character_hit_target(character:Character, target:Character, ability:Ability, effect:Effect) -> bool:
	# Base(50) + skill(weapon skill etc) + bonus(weapon, buffs, effect accuracy etc.)
	var success_chance = 50
	var roll = int(rand_range(1, 100))
	print("hit ", roll)
	return roll <= success_chance

# Check whether a target character is able to dodge
func does_character_dodge(character:Character) -> bool:
	# Base(1) + stat(finesse) + skill(dodge) + Bonus(buffs, armor etc.)
	var success_chance = 1
	var roll = int(rand_range(1, 100))
	print("dodge ", roll)
	return roll <= success_chance

# Check whether a target character is able to block
func does_character_block(character:Character) -> bool:
	# if character.can_block():
	# Base(0) + stat(might) + skill(shields etc) + bonus(buffs, shield etc.)
	var success_chance = 0
	var roll = int(rand_range(1, 100))
	print("block ", roll)
	return roll <= success_chance

# Calculate the damage of an ability effect on a target character and apply the amount to the character
func calculate_and_apply_damage(character:Character, target:Character, ability:Ability, effect:Effect) -> void:
	# DamageDie Roll + stat + skill + bonus
	var damage = Globals.roll_damage_dice(effect.power)
	# Reduced by Resistance
	var resistance = target.get_resistance(effect.type)
	var true_damage = damage - clamp(resistance - effect.penetration, 0, resistance)
	print("damage ", true_damage)
	target.apply_stat_change(Globals.STAT.Health, -true_damage)

# Handle an NPCs turn
func do_npc_turn() -> void:
	var current_turn_obj = get_current_turn_object()
	current_turn_obj.current_ap = 2
	$HUD.update_remaining_ap(current_turn_obj.current_ap)
	
	yield(get_tree().create_timer(0.5), "timeout")
	execute_npc_action()
	current_turn_obj.current_ap -= 1
	$HUD.update_remaining_ap(current_turn_obj.current_ap)
	
	yield(get_tree().create_timer(0.5), "timeout")
	execute_npc_action()
	current_turn_obj.current_ap -= 1
	$HUD.update_remaining_ap(current_turn_obj.current_ap)
	
	end_turn(get_current_acting_character())

# Executes the specified action for the NPC
func execute_npc_action() -> void:
	# TODO: temporary move in random direction
	
	# find enemy
		# if no enemy
			# move randomly
		# else
			# if close enough to hit enemy?
				# hit enemy
			# else
				# move closer to enemy
		
	
	var npc = get_current_acting_character()
	var possible_moves = $TileMap.get_neighbors(npc.grid_position, npc.get_movement_speed(), 
		[Globals.TILE_FILTERS.CanOccupy, Globals.TILE_FILTERS.NoRootTile])
	$TileMap.update_character_position(npc, possible_moves[rand_range(0, possible_moves.size())])
