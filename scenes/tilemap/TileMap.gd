extends Node2D

signal tile_clicked(grid_pos)

# A function reference for highlighting a tile
var highlight_tile_ref:FuncRef = funcref(self, 'highlight_tile')
# The tile scene reference
var tile_scene_ref:Resource = preload("res://scenes/tilemap/Tile.tscn")
# The grid of tiles
var tile_grid:Dictionary = {}
# The set of tile graphical assets organized by name
var tile_set:Dictionary = {}
# The size of the grid
export var grid_size:Vector2 = Vector2(10, 10)
# The size of the tile in pixels
export var tile_size:int = 64
# The margin offset between the tiles and the tile map edge
export var margin:Vector2 = Vector2(1, 1)

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	tile_set["stone_wall"] = "res://assets/Dungeon Crawl Stone Soup Supplemental/dungeon/wall/catacombs_0.png"
	tile_set["grey_dirt"] = "res://assets/Dungeon Crawl Stone Soup Full/dungeon/floor/grey_dirt_0_new.png"
	tile_set["sand_stone"] = "res://assets/Dungeon Crawl Stone Soup Full/dungeon/floor/floor_sand_stone_0.png"
	init_tiles()

# Called when a tile is clicked, then signals up
func _on_tile_clicked(grid_pos:Vector2) -> void:
	emit_signal("tile_clicked", grid_pos)

# Initialize the tiles for the map
func init_tiles() -> void:
	for i in grid_size.x:
		for j in grid_size.y:
			var tile = tile_scene_ref.instance()
			var tile_position = Vector2(i, j)
			# TODO: figure out how to make these names constants
			var info = get_tile_info(i, j)
			var asset = tile_set[info.type]
			add_child(tile)
			tile.init(tile_position, margin, tile_size, info.type, info.canOccupy, asset)
			tile_grid[tile_position] = tile
			tile.connect("tile_clicked", self, "_on_tile_clicked")

# Get the meta data information for a tile
func get_tile_info(x:int, y:int) -> Dictionary:
	if x == 0 or x == (grid_size.x - 1) or y == 0 or y == (grid_size.y - 1):
		return {"type": "stone_wall", "canOccupy": false}
	else:
		return {"type": "grey_dirt", "canOccupy": true}

# Get the neighbors of a particular tile for the specified number of steps out from the root tile
func get_neighbors(grid_pos:Vector2, steps:int, filters:Array = [], transform_function:FuncRef = null) -> Array:
	var neighbors = []
	var root_tile = tile_grid[grid_pos]
	if root_tile != null:
		for i in range(grid_pos.x - steps, grid_pos.x + steps + 1):
			for j in range(grid_pos.y - steps, grid_pos.y + steps + 1):
				var pos = Vector2(i, j)
				if filters.has(Globals.TILE_FILTERS.NoRootTile) and pos == grid_pos:
					continue
				var tile = tile_grid[pos]
				if tile != null:
					if filters.has(Globals.TILE_FILTERS.CanOccupy) and !tile.can_be_occupied():
						continue
					if transform_function != null:
						transform_function.call_func(tile)
					neighbors.append(pos)
	return neighbors

# TODO: Combine with get_neighbors somehow
func get_characters_in_tiles(tile_positions:Array) -> Array:	
	var characters = []
	for tile_position in tile_positions:
		print("pos ", tile_position)
		var tile = tile_grid[tile_position]
		if tile.is_occupied:
			characters.append(tile.occupying_character)
	return characters

# Highlight a given tile
func highlight_tile(tile:Tile) -> void:
	tile.update_asset_texture(tile_set["sand_stone"])

# Reset a tile's graphical texture asset
func reset_tile_textures(tile_positions:Array) -> void:
	for tile_position in tile_positions:
		tile_grid[tile_position].revert_to_original_asset_texture()

# Update the character's position on the grid
func update_character_position(character:Character, grid_pos:Vector2) -> void:
	# if valid -> is one of the neighbors found with movement speed
		# and is a valid tile to move to -> "can_occupy"
	
	# TODO: make the vector2 offset a constant based on 1/2 the sprite size
	if tile_grid.has(grid_pos):
		var prev_tile = character.grid_position
		if tile_grid.has(prev_tile):
			tile_grid[prev_tile].set_unoccupied()
		var tile = tile_grid[grid_pos]
		var char_offset = tile_size / character.scale.x
		character.move(grid_pos, to_global(tile.position) - Vector2(char_offset, char_offset))
		tile.set_occupied(character)
