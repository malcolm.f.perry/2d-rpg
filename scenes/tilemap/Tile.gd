extends Sprite
class_name Tile

signal tile_clicked(grid_pos)

# The tile's position in the grid
var grid_position:Vector2 = Vector2(-1, -1)
# The type of the tile
var tile_type:String = ""
# The path to the tile's graphical asset
var tile_asset_path:String = ""
# Whether the tile can be occupied by anything
var can_occupy:bool = false
# Whether the tile is currently occupied
var is_occupied:bool = false
# The character occupying the tile
var occupying_character:Character = null

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	pass # Replace with function body.

# Called when there is a click event, checks if this tile has been clicked and signals up
func _input(event:InputEvent) -> void:
	if event.is_action_pressed("ui_clicked_left"):
		if get_rect().has_point(to_local(event.position)):
			emit_signal("tile_clicked", grid_position)

# Initialize the tile's variables
func init(grid_pos:Vector2, margin:Vector2, tile_size:int, type:String, new_can_occupy:bool, asset:String) -> void:
	grid_position = grid_pos
	tile_type = type
	can_occupy = new_can_occupy
	tile_asset_path = asset
	set_texture(load(tile_asset_path))
	scale = Vector2(2,2)
	position = (grid_position + margin) * Vector2(tile_size, tile_size)
	update()

# Check if the tile can be occupied
func can_be_occupied() -> bool:
	return can_occupy and !is_occupied

# Set an occupying character onto the tile
func set_occupied(character:Character) -> void:
	is_occupied = true
	occupying_character = character

# Remove an occupying character from the tile
func set_unoccupied() -> void:
	is_occupied = false
	occupying_character = null

# Update the graphical asset of the tile
func update_asset_texture(asset:String) -> void:
	set_texture(load(asset))

# Revert the tile to its originally initialized graphical asset
func revert_to_original_asset_texture() -> void:
	set_texture(load(tile_asset_path))
